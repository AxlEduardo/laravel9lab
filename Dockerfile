FROM dunglas/frankenphp
RUN apt update -y
RUN install-php-extensions \
pdo_mysql \
pdo_pgsql \
gd \
intl \
zip \
opcache
RUN apt-get update -y
RUN apt-get install nano -y
RUN apt-get install iputils-ping -y
RUN apt-get install git -y
RUN apt-get install nodejs -y

COPY --from=composer /usr/bin/composer /usr/bin/composer

ENV SERVER_NAME=:80

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY laravel9lab app
